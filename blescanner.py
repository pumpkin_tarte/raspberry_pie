#!/usr/bin/python3
# coding: utf-8
import blescan3
import bluetooth._bluetooth as bluez
import requests

import argparse
from collections import namedtuple
from functools import reduce
from logging import getLogger, StreamHandler, DEBUG
import sys
import time

logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)

Beacon = namedtuple("Beacon", ["bid", "rssi"])


def parse(s):
  """
  :type s: str
  :rtype: Beacon
  """
  tokens = s.split(",")
  mac_addr = tokens[0].split(":")
  rssi = int(tokens[-1])
  bid = (mac_addr[0] + mac_addr[-1]).upper()
  return Beacon(bid, rssi)


def update_home(now, latest):
  """
  :type now: {str}
  :type latest: [{str}]
  :rtype: ([{str}], {str}, {str})
  latest, enter, leave
  """
  now_devices = reduce(lambda acc,x: acc | x, latest)
  enter = now - now_devices
  log = [now] + latest[:-1]
  next_devices = reduce(lambda acc,x: acc | x, log)
  leave = latest[-1] - next_devices
  return log, enter, leave


def update_server(bid, in_home):
  payload = {
    "beacon_id": bid,
    "in_home": in_home,
    "raspi_num": 1
  }
  requests.post("http://52.198.22.186:5000/raspi/update_home/", json=payload)


def main():
  p = argparse.ArgumentParser()
  p.add_argument("-d", "--device_id", type=int, default=0)
  p.add_argument("-m", "--min_rssi", type=int, default=-60)
  p.add_argument("-l", "--log_size", type=int, default=5)
  p.add_argument("-t", "--time", type=int, default=1000)
  args = p.parse_args()

  try:
    sock = bluez.hci_open_dev(args.device_id)
  except:
    sys.exit(-1)
  blescan3.hci_le_set_scan_parameters(sock)
  blescan3.hci_enable_le_scan(sock)

  devices_log = [set() for _ in range(args.log_size)]
  while True:
    try:
      beacons = [parse(e) for e in blescan3.parse_events(sock, 10)]
    except OSError as e:
      pass
    else:
      logger.info("All Device: {}".format(reduce(lambda acc,x: acc | x, devices_log)))
      logger.info("Beacons: {}".format(beacons))
      devices = {beacon.bid for beacon in beacons if beacon.rssi > args.min_rssi}
      devices_log, enter, leave = update_home(devices, devices_log)

      logger.info("Enter: {}".format(enter))
      for bid in enter:
        update_server(bid, True)

      logger.info("Leave: {}".format(leave))
      for bid in leave:
        update_server(bid, False)
    time.sleep(args.time / 1000.0)

if __name__ == "__main__":
  main()
