# test BLE Scanning software
# jcs 6/8/2014

import blescan3
import sys

import bluetooth._bluetooth as bluez
import requests
import json
import time
from datetime import datetime
from functools import reduce


def parse(s):
  splited = s.split(",")
  mac = splited[0].split(":")
  rssi = int(splited[-1])
  bid = (mac[0] + mac[-1]).upper()
  return bid, rssi

def update_home(now, latest):
  """
  :type now: {str}
  :type latest: [{str}]
  :rtype: ([{str}], {str}, {str})
  latest, enter, leave
  """
  now_devices = reduce(lambda acc,x: acc | x, latest)
  enter = now - now_devices
  log = [now] + latest[:-1]
  next_devices = reduce(lambda acc,x: acc | x, log)
  leave = latest[-1] - next_devices
  return log, enter, leave

dev_id = 0
try:
  sock = bluez.hci_open_dev(dev_id)
  print ("ble thread started")

except:
  print ("error accessing bluetooth device...")
  sys.exit(1)

blescan3.hci_le_set_scan_parameters(sock)
blescan3.hci_enable_le_scan(sock)

old_devices = [set(), set(), set(), set(), set()]
#new_devices = []
while True:
  try :
    beacons = [parse(e) for e in blescan3.parse_events(sock, 10)]
  except OSError as err :
    print("OS error: {0}".format(err))
  else :
    print ("----------")
    new_devices = {beacon[0] for beacon in beacons if beacon[1] > -60}
#    new = []
#    miss = []
    
#    for beacon in new_devices-old_devices:
#    print("+ {0:%Y/%m/%d %H:%M:%S}, MACaddr:{1}, RSSI:{2}".format(datetime.now(), beacon[0:17], beacon[-3:]))
#    new += [[beacon[0:2] + beacon[15:17]], beacon[-3:]]
#    for beacon in old_devices-new_devices:
#    print("- {0:%Y/%m/%d %H:%M:%S}, MACaddr:{1}, RSSI:{2}".format(datetime.now(), beacon[0:17], beacon[-3:]))
#    miss += [[beacon[0:2] + beacon[15:17]], beacon[-3:]]
#    print("**********")
    old_device = reduce(lambda acc,x: acc | x, old_devices)
    for addr in new_devices-old_device:
      print("+ {0:%Y/%m/%d %H:%M:%S}, MACaddr:{1}".format(datetime.now(), addr))
      #print(new_devices)
      new = addr
      print(new)
      data = {
        "beacon_id":new,
        "in_home":True,
        "raspi_num":1
      }
      res = requests.post("http://52.198.22.186:5000/raspi/update_home/", json=data)
#      print(res)
#      print(res.text)
    old_device = reduce(lambda acc,x: acc | x, old_devices[:-1]) | new_devices
    for addr in old_devices[-1] - old_device:
      print("- {0:%Y/%m/%d %H:%M:%S}, MACaddr:{1}".format(datetime.now(), addr))
      #print(pld_devices)
      miss = addr
      data = {
        "beacon_id":miss,
        "in_home":False,
        "raspi_num":1
      }
      res = requests.post("http://52.198.22.186:5000/raspi/update_home/", json=data)
#      print(res)
#      print(res.text)

    print("new ", new_devices)
    print("old ", old_devices)
    old_devices = [new_devices] + old_devices[:-1]
    time.sleep(1)
